from flask import Flask
from flask_jwt import JWT
from flask_restful import Api

from pythonorm.resources.item import Items, ItemList
from pythonorm.resources.store import Store, StoreList
from pythonorm.security import authenticate, identity
from pythonorm.resources.user import UserRegister

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'
#tell not to use flask sql track update alchamy
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.secret_key = 'jose'
api = Api(app)

@app.before_first_request
def create_tables():
    db.create_all()

jwt = JWT(app, authenticate, identity)  # /auth

api.add_resource(Store, '/store/<string:name>')
api.add_resource(StoreList, '/stores')
api.add_resource(Items, '/item/<string:name>')
api.add_resource(ItemList, '/items')
api.add_resource(UserRegister, '/register')
if __name__ == '__main__':
    from pythonorm.db import db
    db.init_app(app)
    app.run(port=80, debug='true')
