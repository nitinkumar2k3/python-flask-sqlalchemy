from flask_jwt import jwt_required
from flask_restful import reqparse, Resource

from pythonorm.models.item import ItemModel


class Items(Resource):
    TABLE_NAME = 'items'
    parser = reqparse.RequestParser()
    parser.add_argument('price',
                        type=float,
                        required=True,
                        help="This cant be blank"
                        )


    parser.add_argument('store_id',
                    type=int,
                    required=True,
                    help="Every item needs store id"
                    )


    @jwt_required()
    def get(self, name):
        item = ItemModel.find_by_name(name)
        if item:
            return item.json()
        return {'message': 'Item not found'}, 404


    def post(self, name):
        if ItemModel.find_by_name(name):
            return {'message': 'An item is present with {} name'.format(name)}, 400

        data = Items.parser.parse_args()

        item = ItemModel(name, **data)
        try:
            item.save_to_db()
        except:
            return {"message": "An error occurred inserting the item."}, 500

        return item.json()


    def delete(self, name):
        item = ItemModel.find_by_name(name)
        if item:
            item.delete_from_db()
            return {'message': 'Item deleted.'}
        return {'message': 'Item not found.'}, 404


    def put(self, name):
        data = Items.parser.parse_args()
        item = ItemModel.find_by_name(name)
        if item:
            item = ItemModel(name, **data)
        else:
            item.price = data['price']

        item.save_to_db()

        return item.json()


class ItemList(Resource):
    TABLE_NAME = 'items'

    def get(self):
        # Usinh list compherison
        return {'items': [item.json() for item in ItemModel.query.all()]}
    # using lamda
    # return {'items': list(map(lambda x: x.json(), ItemModel.query.all()))}
